from .process import DocumentPartProcessTestCase
from .share import DocumentShareTestCase
from .tasks import TasksTestCase
from .views import DocumentTestCase

__all__ = [
    DocumentTestCase,
    DocumentShareTestCase,
    DocumentPartProcessTestCase,
    TasksTestCase
]
